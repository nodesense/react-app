## Install

We need Node.js 6.x/6.x to be installed. 
Clone this respository, then

> npm install

## Webpack Server

> npm start


## Webpack Production

> npm run build

Bundles are kept in dist folder

## API Server

You should run the server, API Server code with instruction is found at https://github.com/nodesense/restful-server
 
For login page, use below credential

username: admin, password: admin

username: staff, password: staff

username: user, password: user